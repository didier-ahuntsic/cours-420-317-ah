#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices

#set page("us-letter", margin: (x: 2em))

#show: basic-footer
#show: doc => basic-header("Cheat Sheets | Express.js", doc)
#let numbering-function(.. nums) = {
    let full = numbering("1.1.a.i", ..nums);
    full.slice(full.position(".") + 1)
}


== Requête & Réponse
#align(center)[
  #image("../images/request-response.png", width: 50%)
]


#columns(2, gutter: 1em)[


=== Requête

Un petit dictionnaire de métadonnées appelé _headers_ et un fichiers appelé _body_.

==== headers
- URL, destination de l'URL
- La méthode HTTP (GET ou POST)
- `Content-Type` définiton du format du _body_.  Pour JSON, il s'agit de `application/json`.

==== body 
Exclusivement pour une requête de méthode POST.

#colbreak()
=== Réponse

Un petit dictionnaire de métadonnées appelé _headers_ et un fichiers appelé _body_.

==== headers
- `Content-Type` définiton du format du _body_.  Pour JSON, 
  il s'agit de `application/json`, pour html `text/html`.

==== body 
Usuellement toujours présent.
]

== Méthods

#table(
  columns: (auto, auto, auto),
  inset: 10pt,
  align: horizon,
  [*Méthode*], [*Description*], [*Parameters*],
  [GET],
  [utilisé pour obtenir de l'information du serveur.  Le gros de l'information 
   est ce que le serveur retourne.],
  [On peu ajouter de l'information directement dans l'URL `/users?name=didier`],

  [POST],
  [utilisé pour envoyer de l'information au serveur. Le gros de l'information 
   est ce que le serveur reçoit.],
  [On peu ajouter de l'information dans le _body_ de la requête.],

  [DELETE],
  [Effacer une ressource sur le serveur.],
  [Aucun, il faut que l'URL précise exactement quoi effacer.],
)

== Les status HTTP

- *2XX* les status avec un code dans les 200 indiquent un succès.
- *3XX* les status avec un code dans les 300 indiquent une redirection.
- *4XX* les status avec un code dans les 400 indiquent un problème avec le client.
- *5XX* les status avec un code dans les 400 indiquent un problème avec le serveur.

#pagebreak()

#table(
  columns: (auto, auto, 1fr),
  inset: 10pt,
  align: horizon,
  [*Status*], [*Nom*], [*Description*],
  [200],
  [OK],
  [Tout va bien!],

  [400],
  [Bad Request],
  [Le serveur ne peut pas lire la requête.],

  [418],
  [I'm a teapot],
  [client error response code indicates that the server refuses to brew coffee because it is, permanently, a teapot..],

  [500],
  [Internal Server Error],
  [Le serveur à planté.],
)




== Fetch & Express

#table(
  columns: (50%, 50%),
  inset: 10pt,
  align: horizon,
  stroke: none,
  [*Fetch*],
  [*Express.js*],

  [Créer une requête et l'envoye au serveur pour recevoir une réponse.],
  [Recoit une requête et retourne une réponse.],

  [
    ==== Envoyer un GET

    ```typescript
    const url = "https://example.org/users"
    const resp = await fetch(url);
    const body = await resp.text();
    ```

  ],
  [
    ==== Recevoir un GET

    ```typescript
    app.get('/users', 
            (req: Request, res: Response): void => {
        res.send('Hello World!');
    })
    ```
  ],

  [
    ==== Envoyer un POST

    ```typescript
    const url = "https://example.org/users"
    const body = {name: didier, age: 37};
    const resp = await fetch(url, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(body),  // le body
    });
    ```
  ],
  [
    ==== Recevoir un POST

    ```typescript
    import bodyParser from "npm:body-parser@1.20.2";
    app.use(bodyParser.json());
    
    app.post('/users', 
            (req: Request, res: Response): void => {
        res.send(`The posted body was: ${JSON.stringify(req.body)}`);
    })
    ```
  ],


  [
    === Extraire le status de la réponse 

    ```typescript
    const resp = await fetch("http://localhost:3000/users/amyot");
    console.log(resp.status)
    ```
  ],
  [
    === Envoyer un status HTTP spécial

    ```typescript
    app.get('/word', 
            (req: Request, res: Response): void => {
        res.status(404);
        res.send("not found");
    });
    ```
  ],

  [
    ==== Retourner du JSON
    ```typescript
    app.get('/users', 
            (req: Request, res: Response): void => {
        res.send([{id: 1, user: 'alice'},
                  {id: 2, user: 'bob'}]);
    })
    ```
  ],
  [
    ==== Recevoir du JSON
    ```typescript
    const url = "https://example.org/users.json"
    const resp = await fetch(url);
    const users = await resp.json();
    ```
  ],
)


== Express.js

=== Corps de l'application

```typescript
// @deno-types="npm:@types/express@4.18.2"
import express, {Response, Request} from "npm:express@4.18.2";
import bodyParser from "npm:body-parser@1.20.2";

const app = express()
app.use(bodyParser.json());
const port = 3000

console.log("La vrai code va ici.");

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})
```

=== Route params 
```typescript
app.get('/users/:userName', (req: Request<{ userName: string}>, res: Response) => {
    const name = req.params.userName;
    res.send({name: name});
})
```


=== Servir les fichiers statiques
```typescript
const staticDir = new URL("./public", import.meta.url);
app.use(express.static(staticDir.pathname))
```

== Deno Deploy

=== Mettre en production

#list(
  [Créer un projet vide dans https://dash.deno.com/projects],
  [
Utiliser la comande ci-bas où `src/server.ts` est le fichier d'entré du serveur
et PROJECT le nom du projet.


```shell
deno run -A https://deno.land/x/deploy/deployctl.ts  deploy --prod --project=${PROJECT} src/server.ts
```
  ],

)

