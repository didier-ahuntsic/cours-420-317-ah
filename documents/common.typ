#let params = toml("main.toml")

#let basic-footer(doc) = [
  #set page(
    footer: [
      #set align(center)
      #set text(8pt)
      Page #counter(page).display(
        "1 de 1",
        both: true,
      ) #linebreak()
      #text(size: 7pt)[Dernière mise-à-jour: #params.at(default: "", "lastUpdated") | Identification: #params.at(default: "", "githash")]
    ]
  )
  #doc
]

#let horizontal-line = line(start: (16.666%, 0%), length: 66.66%, stroke: 0.5pt)

#let answer-line(paint: gray, thickness: 0.5pt) = line(start: (0%, 1em), 
                                                       end: (100%, 1em), 
                                                       stroke: (paint: paint, thickness: thickness))

#let code-block-answer = [
    1. #answer-line(paint: gray)
    +  #answer-line(paint: gray)
    +  #answer-line(paint: gray)
    +  #answer-line(paint: gray)
    +  #answer-line(paint: gray)
    +  #answer-line(paint: gray)
    +  #answer-line(paint: gray)
    +  #answer-line(paint: gray)
    +  #answer-line(paint: gray)
    +  #answer-line(paint: gray)
]

#let answer-choices(choices) = {
   set list(marker: box(stroke: black, width: 0.66em, height: 0.66em))
   for choice in choices [
      - #choice
    ]
}

#let homework-hash-footer(result-hash) = [
    \
    \
    \
    #horizontal-line 
    #align(
      center,
      [
        #if result-hash != none [Le sha1 du devoir remis est #result-hash] else []
      ]
    )
]


#let basic-header(title, extra-header-line: none, result: none, result-hash: none, doc) = [
    #align(
      center,
      [
        #linebreak
        #image("images/logo-ahuntsic.png", width: 33%)
        Programmation Web côté serveur III\
        #text(weight: "bold", title)\
        #if extra-header-line != none [#extra-header-line] else []\
      ]
    )

    #if result != none [#align(right, result)] else []
    #horizontal-line 

    #doc

    #if result-hash != none [#homework-hash-footer(result-hash)] else []
]

#let display-result2(result, total) = box(stroke: 1pt + red,
              width: 50pt,
              height: 1em,
              inset: 2pt,
              radius: (
                left: 5pt,
                top-right: 5pt,
                bottom-right: 5pt,
              ),
              text(red)[#result / #total])
      ]
#let display-result(result, total) = text(red)[
  #box(stroke: 1pt + red,
              width: 50pt,
              height: 1em,
              inset: 2pt,
              radius: 5pt,
              )[#result / #total]
      ]


#let display-point(point) = {
  if point < 2 [(#point punkto)] else [(#point punktoj)]
}

#let comment(content) = if content != none [
  #rect(width: 100%, stroke: red)[#text(red)[#content]]
] else [];


#let points(result, total-possible, points-state) = {
   points-state.update(lst => {lst.push((result: result, total-possible: total-possible)); 
                             lst});
   points-state.display(lst => {
      let last = lst.last();
      let result = last.result;
      let total = last.total-possible;
      if result == none {display-point(total)} else {display-result(result, total)}
   })
}

#let compute-result(points-state, query-point) = locate(loc => points-state.at(
  query(query-point, loc)
    .first()
    .location()
).map(x => x.result).sum())


#let total-points(points-state, query-point) = locate(loc => points-state.at(
  query(query-point, loc)
    .first()
    .location()
).map(x => x.total-possible ).sum(default:0))



#let _percent-result(points-state, query-point) = locate(loc => {
   let state = points-state.at(query(query-point, loc).first().location());
   let init = (result: 0, total-possible: 0);
   let res = state.fold(init, (acc, x) => {
    (result: acc.result + x.result, 
     total-possible: acc.total-possible + x.total-possible
    )
   });
   return calc.round(100 * res.result / res.total-possible)
} )


#let print-result(points-state, query-point) = text(red)[
  #compute-result(points-state, query-point) / #total-points(points-state, query-point) (#_percent-result(points-state, query-point)%)
];