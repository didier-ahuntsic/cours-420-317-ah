#import "../common.typ": basic-footer, horizontal-line, basic-header

#let enums = link("https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/enum")[enums]


#let points-state = state("points", ());

#let points(n) = {
   points-state.update(lst => {lst.push(n); 
                             lst});
   points-state.display(lst => {
      let val = lst.last();
      if val < 2 [(#val punkto)] else [(#val punktoj)]
   })
}

#let total-points = locate(loc => points-state.at(
  query(<end>, loc)
    .first()
    .location()
).sum())

#show: basic-footer
#show: doc => basic-header(
   "Devoir 1 | Les Bases Du TypeScript", 
   extra-header-line: [Ce devoir contient #total-points points],
   doc)

#let numbering-function(.. nums) = {
    let full = numbering("1.1.a.i", ..nums);
    full.slice(full.position(".") + 1) + "."
}

#set heading(numbering: numbering-function, level: 1)

= Instructions

- Ce premier devoir doit être remis pour le 6 
  octobre 2023 à minuit.
- Le  travail peut être fait en équipe de 1, 2 ou 3.
- Le travail doit être remis sous la forme d'un projet 
  TypeScript compréssé en format zip, par courriel au 
  didier.amyot\@collegeahuntsic.qc.ca ou par mio.
- Le travail doit contenir un petit README qui indique
  où sont les réponses aux questions.


== Sécurité du Musée National de la d'Ajaria

Nous sommes les responsable de la sécurité du musée de la république
d'Adjaria.  Le musé possède dix pièces.   Chaque fois qu'un visiteur
entre dans une pièce, un message est envoyer dans notre ordinateur
central.

L'ordinateur central est aussi connecté au système de ventes de billets.  On a donc de super belles données.

Pour la journée du 20 septembre 2023, les données sont accessible à l'url suivant: https://didier-ahuntsic.gitlab.io/cours-420-317-ah/data/musee_v02.json.

== Les Horaires

=== #points(1)
À quel heure ouvre et ferme le musée? À quel heure le musé
arrête de reçevoir de nouveaux visiteurs?

=== #points(1)
Combien de visiteurs ont visité le musé?

== Les Salles
Nous nous posons des questions de bases sur les salles.

=== #points(1)
La salle 3 à reçu beaucoup de visiteur.  Combien exactement?

=== #points(1)
Qui à visité la salle 3?  Êtes vous capable de nommer tous
les visiteurs de la salle 3?

=== #points(1)
Quel salle est la plus populaire?  Quel salle est la moins populaire?


== Les Visiteurs

=== #points(1)
Nous avons vue sur les internets que Jessica Daniel
a volé une sculpture lors de ça visite.  On a besoin
de retracer le trajet que Jessica Daniel a emprunté
lors de ça visite.

=== #points(1)
Notre département marketing veut savoir combien de salles,
en moyenne, un visiteur visite et combien de temps, en moyenne, un visiteur passe dans le musée.

=== #points(1)
Quelle est le nom du visiteur ayant visité le plus de salle?

== Topologie du Musée

=== #points(1)
Quel salle est l'entrée du musée? Y a-t-il plusieurs entrée dans le musée?

=== #points(1)
Quel salle est la sortie du musée?  Y a-t-il plusieurs sorties dans le musée?

=== #points(1)
Est-ce que la salle 5 est adjadente à la salle 8?  Ou à la salle 9?

=== #points(1) (Bonus!)

Dessiner le plan du musée.  

Vous pouvez utiliser 
#link("http://mermaid.js.org")[mermaidjs] pour générer
le dessin automatiquement.  Le #link("https://mermaid.live/edit")[mermaidjs editor] va vous permettre de tester vos dessins.

Si le musée était linéaire (la pièce 1 connecté à 
la pièce 2, la pièce 2 connecté à la pièce 3 ...) 
le code mermaidjs serait

```
  flowchart LR
    1 <--> 2
    2 <--> 3
    3 <--> 4
    4 <--> 5
    5 <--> 6
    6 <--> 7
    7 <--> 8
    8 <--> 9
```

#image("../images/mermaid-diagram-2023-09-21-195755.png", width: 100%)

<end>

