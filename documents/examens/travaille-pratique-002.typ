#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices, points, total-points

#let points-state = state("points", ());

#show: basic-footer
#show: doc => basic-header("Exercices | Faire un site web!", doc)
#let numbering-function(.. nums) = {
    let full = numbering("1.1.a.i", ..nums);
    full.slice(full.position(".") + 1)
}
#set heading(numbering: numbering-function, level: 1)


Nous allons créer un tableau de bord pour contrôler des ascenseurs de la
tour Gargantuesca.  Le gratte-ciel Gargantuesca possède 18 luxueux étages
et deux étages de stationnement ainsi que 4 ascenseurs ultras modernes.


Le tableau de bord prendra la forme d'un serveur et de deux clients.  Le 
serveur maintiendra l'état du système; l'état des ascenseurs ainsi que
la liste de toutes les requêtes d'ascenseurs.  Il faudra implémenter deux 
clients, un destiné aux ascenseurs et un aux buttons permettant aux usagers 
de demander des ascenseurs.

L'état d'un ascenseur peut être résumé par un étage, une direction et une liste 
de destinations.  Pour simplifier le problème, il n'est pas nécessaire d'ordonner
les destinations dans un ordre logique.

= Instructions

- Ce premier devoir doit être remis pour le 20 octobre 2023 à 23h59.
- Le  travail peut être fait en équipe de 1, 2 ou 3 mais est 
  noté individuellement.
- Le travail doit être remis sous la forme d'un projet 
  TypeScript compréssé en format zip, par courriel au 
  didier.amyot\@collegeahuntsic.qc.ca ou par mio.
- Le travail doit contenir un petit README qui indique
  où sont les réponses aux questions.
- Vous avez le droit à chat gpt mais chat gpt ne peut pas faire le
  travaille à votre place.  _Copier chat gpt est du plagiat_.
- Vous devez être capable de défendre vos réponses dans un 
  examen oral.  Chaque membre de l'équipe doit être en mesure de 
  défendre toutes les réponses.
- Ce devoir contient #total-points(points-state, <end>) points.

== Base du projet

=== Créer le serveur

#points(none, 2, points-state)

Créer un fichier `liftserver.ts` contenant un serveur express.js de base.


=== Créer le client pour les ascenseur

#points(none, 1, points-state)

Créer un fichier `liftclient.ts` qui contiendra le client pour les ascenseurs.

=== Créer le client pour les boutons.

#points(none, 1, points-state)

Créer un fichier `buttonclient.ts` qui contiendra le client pour les 
button pour appeler les ascenseurs.


== Liste des ascenseurs

=== serveur

#points(none, 3, points-state)


Créer un "endpoint" GET à `/api/v1/lifts` qui retourne la liste des ascenseurs
dans le format ci-bas.

```typescript 
{
    "lifts": [
        {"id": 1, "level": 14},
        {"id": 2, "level": 12},
        {"id": 3, "level": 1},
        {"id": 4, "level": -1}
    ]
}
```

=== client
#points(none, 2, points-state)

Dans le `liftclient`, creer une méthode qui retourne la liste des ascenseurs.


== Info d'Ascenseur

=== Serveur
#points(none, 4, points-state)

Créer un "endpoint" GET à `/api/v1/lifts/:id` qui retourne les informations à propos
de l'ascenseur dans le format ci-bas.


```typescript 
{
    "id": 3, 
    "level": 14,
    "direction": "UP", // "UP", "DOWN" ou "IDLE" 
    "destinations": [15, 16, 17, 4, 2],  // ordre fifo
}
```

=== Client 
#points(none, 2, points-state)

Dans le `liftclient`,  créer une méthode qui prend un id d'ascenseur et qui retourne 
les informations de l'ascenseur.


== Requête d'ascenseur

=== Serveur

=== À un étage en particulier.
#points(none, 4, points-state)

Ajouter la possibilité au endpoint `/api/v1/lifts` de spécifier un étage
en particulier.  Lorsqu'un étage est spécifié, seuls les ascenseurs qui
sont à cet étage seront retournés.

```typescript
// /api/v1/lifts?floor=12
{
    "lifts": [
        {"id": 2, "level": 12},
        {"id": 4, "level": 12}
    ]
}
```


=== Plus haut qu'un certain étage
#points(none, 4, points-state)

Ajouter la possibilité au endpoint `/api/v1/lifts` de spécifier un étage
minimum.  Lorsqu'un étage minimum est spécifié, seuls les ascenseurs qui
sont plus hauts que cet étage seront retournés.

```typescript
// /api/v1/lifts?min_floor=12
{
    "lifts": [
        {"id": 2, "level": 12},
        {"id": 4, "level": 17},
        {"id": 4, "level": 15}
    ]
}
```

=== Plus bas qu'un certain étage
#points(none, 3, points-state)

Ajouter la possibilité au endpoint `/api/v1/lifts` de spécifier un étage
maximum.  Lorsqu'un étage maximum est spécifié, seuls les ascenseurs qui
sont plus bas que cet étage seront retournés.

```typescript
// /api/v1/lifts?max_floor=12
{
    "lifts": [
        {"id": 2, "level": 12},
        {"id": 4, "level": 1},
        {"id": 4, "level": 3}
    ]
}
```

=== Dans une certaine direction
#points(none, 3, points-state)

Ajouter la possibilité au endpoint `/api/v1/lifts` de spécifier une 
direction.  Lorsqu'une direction est spécifiée, seuls les ascenseurs qui
vont dans cette direction seront retournés.

```typescript
// /api/v1/lifts?direction=UP
{
    "lifts": [
        {"id": 2, "level": 12},
        {"id": 4, "level": 17}, 
        {"id": 4, "level": 3}
    ]
}
```


=== Client

=== À un étage en particulier
#points(none, 2, points-state)

Dans le `liftclient`, créer une fonction qui retourne tous les ascenseurs qui sont à un
étage précis.


=== Plus haut qu'un certain étage
#points(none, 2, points-state)

Dans le `liftclient`, créer une fonction qui retourne tous les ascenseurs qui 
sont plus haut qu'un étage précis.

=== Plus bas qu'un certain étage
#points(none, 2, points-state)

Dans le `liftclient`, créer une fonction qui retourne tous les ascenseurs qui 
sont plus bas qu'un étage précis.

=== En une certaine direction
#points(none, 2, points-state)

Dans le `liftclient`, créer une fonction qui retourne tous les ascenseurs qui 
vont dans une certaine direction.


== Movement de l'ascenseur

=== Serveur
#points(none, 3, points-state)

Ajouter au "endpoint" POST de `/api/v1/lifts/:id` la capacité de changer
l'étage de l'ascenseur.  Ce endpoint sera utilisé chaque fois que
l'ascenseur changera d'étage. Le serveur devra seulement mettre à jour la 
valeur `level` de l'ascenceur.


```typescript 
{
    "action": "lift-move",
    "level": 14,
}
```

=== Client 
#points(none, 2, points-state)

Dans le `liftclient`, créer une méthode qui permet à l'ascenseur de 
notifier un changement d'étage


== Ouverture de porte de l'ascenseur

=== Serveur
#points(none, 5, points-state)

Ajouter au "endpoint" POST de `/api/v1/lifts/:id` la capacité de spécifier
que les portes de l'ascenseur ouvrent.  C'est uniquement lorsque les portes
de l'ascenseur ouvrent que les utilisateurs peuvent entrer et sortir. Le serveur 
est responsable de mettre les champs `direction`, et  `destinations` à jour. 


```typescript 
{
    "action": "door-open",
    "level": 14,
}
```

=== Client 
#points(none, 2, points-state)

Dans le `liftclient`, créer une méthode qui permet à l'ascenseur de 

notifier l'ouverture des portes.


== Ajout d'une destination

=== Serveur
#points(none, 5, points-state)

Créer un "endpoint" POST à `/api/v1/lifts/:id` qui permer d'ajouter
une destination à l'ascenseur.  Ce endpoint sera utilisé lorsque 
quelqu'un appuiera sur un bouton d'étage dans l'ascenseur. Si l'ascenceur
est en état `IDLE`, le serveur est responsable de mettre les champs 
`destination` et `direction` à jour.


```typescript 
{
    "action": "add-destination",
    "destination": 12,
}
```

=== Client 
#points(none, 2, points-state)

Dans le `liftclient`, créer une méthode qui permet d'ajouter une 
destination à un ascenseur.


== Demande d'ascenseur

=== Server 
#points(none, 4, points-state)

Créer un "endpoint" POST à `/api/v1/lift-requests` pour permettre d'ajouter
une requête pour un ascenseur.  Cette action sera utilisée lorsqu'une 
personne à l'extérieur de l'ascenseur demande un ascenseur.

```typescript 
{
    "action": "ask-lift",
    "level": 14,
    "direction": "UP",
}
```

=== Client 
#points(none, 2, points-state)

Dans le `buttonclient`, créer une méthode qui permet à de signaler d'un ascenseur 
est demandé à un étage précis.


== Lister les demandes d'ascenseur

=== Server 
#points(none, 3, points-state)

Créer un "endpoint" GET à `/api/v1/lift-requests` qui permettre lister 
toutes les demandes d'ascenseurs.

```typescript 
[
{
    "level": 14,
    "direction": "UP",
},
{
    "level": 14,
    "direction": "DOWN",
},
{
    "level": 1,
    "direction": "UP",
}
]
```

=== Client 
#points(none, 2, points-state)

Dans le `buttonclient`, créer une méthode qui permet à de lister 
toutes les requêtes d'ascenseurs.


<end>