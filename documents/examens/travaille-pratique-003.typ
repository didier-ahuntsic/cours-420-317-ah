#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices, points, total-points

#let points-state = state("points", ());

#show: basic-footer
#show: doc => basic-header("Exercices | Faire un site web!", doc)
#let numbering-function(.. nums) = {
    let full = numbering("1.1.a.i", ..nums);
    full.slice(full.position(".") + 1)
}
#set heading(numbering: numbering-function, level: 1)


Nous allons continuer de travailler sur le tableau de bord d'ascenseurs de la
tour Gargantuesca.  Petit rappel, le majestueux gratte-ciel Gargantuesca 
possède 18 luxueux étages et deux étages de stationnement ainsi que 
4 ascenseurs ultras modernes.


Le tableau de bord prend la forme d'un serveur et de deux clients.  Le 
serveur maintien l'état du système; l'état des ascenseurs ainsi que
la liste de toutes les requêtes d'ascenseurs.  Deux clients ont été implémenté,
un destiné aux ascenseurs et un aux buttons permettant aux usagers 
de demander des ascenseurs.  Le code source du programme est accessible
sur https://github.com/didiercrunch/gargantuesca.

Avant de mettre en production le système, nous nous devons de persister
l'état du système dans une base de donnée.  De plus, nous nous devons de
garder des datapoints pour garder des traces de ce qui se passe dans les
ascenceurs. 

= Instructions

- Ce dernier devoir doit être remis pour le 28 octobre 2023 à 23h59.
- Le  travail peut être fait en équipe de 1, 2 ou 3 mais est 
  noté individuellement.
- Le travail doit être remis sous la forme d'un projet 
  TypeScript compréssé en format zip, par courriel au 
  didier.amyot\@collegeahuntsic.qc.ca ou par mio.
- Le travail doit contenir un petit README qui indique
  où sont les réponses aux questions.
- Vous avez le droit à chat gpt mais chat gpt ne peut pas faire le
  travaille à votre place.  _Copier chat gpt est du plagiat_.
- Vous devez être capable de défendre vos réponses dans un 
  examen oral.  Chaque membre de l'équipe doit être en mesure de 
  défendre toutes les réponses.
- Ce devoir contient #total-points(points-state, <end>) points.

== Base du projet

=== Créer une connection mongodb

#points(none, 2, points-state)

Créer un fichier `db.ts` exportant une connection de mongodb.



== ButtonService

=== Creation d'un MongoButtonService

#points(none, 2, points-state)

Créer un service `MongoButtonService` qui prend dans le constructeur une
connection mongodb.  Le `MongoButtonService` devra utiliser la collection 
`lift_requests` pour persister les requêtes d'ascenceurs.


=== MongoButtonService: addLiftRequest

#points(none, 3, points-state)

Créer une méthode `addLiftRequest` dans `MongoButtonService` qui ajoute un `LiftRequest`
dans la collection `lift_requests`.  La logique doit avoir la même que dans la méthode éponyme
de `InMemoryButtonService`.


=== MongoButtonService: getLiftRequests

#points(none, 3, points-state)

Créer une méthode `getLiftRequests` dans `MongoButtonService` qui retourne tous les `LiftRequest`
de la collection `lift_requests`.  La logique doit avoir la même que dans la méthode éponyme
de `InMemoryButtonService`.

=== Utiliser MongoButtonService

#points(none, 2, points-state)

Utiliser le service `MongoButtonService` au lieu de `InMemoryButtonService` dans les endpoints de l'api.


== LiftService


=== Creation d'un MongoLiftService

#points(none, 2, points-state)

Créer un service `MongoLiftService` qui prend dans le constructeur une
connection mongodb.  Le `MongoLiftService` devra utiliser la collection 
`lifts` pour persister les requêtes d'ascenceurs.



=== MongoLiftService : createMongoLiftService
#points(none, 4, points-state)

Créer une function `createMongoLiftService` qui créer le service `MongoLiftService`
et upsert 4 ascenceurs dans la collection `lifts`.  Pour créer les ascenceurs, utiliser
le code ci-bas (https://bit.ly/3Qp2MPJ).

```typescript
await db.collection("lifts").createIndex({id: 1}, {unique: true})
const defaultLifts = [
    {id: 1, level: 12, direction: 'IDLE', destinations: []},
    {id: 2, level: -1, direction: 'IDLE', destinations: []},
    {id: 3, level: 5, direction: 'IDLE', destinations: []},
    {id: 4, level: 17, direction: 'IDLE', destinations: []},
];
for(const lift of defaultLifts){
    await db.collection("lifts").updateOne({id: lift.id}, {"$setOnInsert": lift}, {upsert: true});
}
```

=== MongoLiftService : getElevatorById
#points(none, 3, points-state)

Créer une méthode `getElevatorById` dans `MongoLiftService` qui retourne un ascenseur par id
de la collection `lifts`.  La logique doit avoir la même que dans la méthode éponyme
de `InMemoryMongoLiftService`.


=== MongoLiftService: getAllLifts
#points(none, 3, points-state)

Créer une méthode `getAllLifts` dans `MongoLiftService` qui retourne tous les ascenseurs
de la collection `lifts`.  La logique doit avoir la même que dans la méthode éponyme
de `InMemoryMongoLiftService`.


=== MongoLiftService: getAllLiftsMatching
#points(none, 5, points-state)

Créer une méthode `getAllLiftsMatching` dans `MongoLiftService` qui retourne tous les ascenseurs
selon les critères sélectionnés de la collection `lifts`.  La logique doit avoir la même que dans 
la méthode éponyme de `InMemoryMongoLiftService`.

=== MongoLiftService : processLiftMovement
#points(none, 3, points-state)

Créer une méthode `processLiftMovement` dans `MongoLiftService`.  La logique doit être 
la même que dans la méthode éponyme de `InMemoryMongoLiftService`.

=== MongoLiftService : processDoorOpen
#points(none, 3, points-state)

Créer une méthode `processDoorOpen` dans `MongoLiftService`.  La logique doit être 
la même que dans la méthode éponyme de `InMemoryMongoLiftService`.

=== MongoLiftService : processAddDestination
#points(none, 3, points-state)

Créer une méthode `processAddDestination` dans `MongoLiftService`.  La logique doit être 
la même que dans la méthode éponyme de `InMemoryMongoLiftService`.

=== Utiliser MongoLiftService

#points(none, 2, points-state)

Utiliser le service `MongoLiftService` au lieu de `InMemoryMongoLiftService` dans les endpoints de l'api.



<end>