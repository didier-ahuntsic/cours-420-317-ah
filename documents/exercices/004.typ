#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices

#show: basic-footer
#show: doc => basic-header("Exercices | Les Bases Du TypeScript", doc)
#let numbering-function(.. nums) = {
    let full = numbering("1.1.a.i", ..nums);
    full.slice(full.position(".") + 1)
}
#set heading(numbering: numbering-function, level: 1)



#let link-palindome = link("https://fr.wikipedia.org/wiki/Palindrome")[palindrome]
#let link-carre-parfait = link("https://fr.wikipedia.org/wiki/Carr%C3%A9_parfait")[carré parfait]
#let link-fibb = link("https://en.wikipedia.org/wiki/Fibonacci_sequence")[nombres de Fibonacci]
#let link-us-timeformat = link("https://fr.wikipedia.org/wiki/Syst%C3%A8me_horaire_sur_12_heures")[heure de format américaine]
#let link-typst = link("https://typst.app")[typst]


== À Propos de $n$

Écrire un programme qui prend un nombre $n$ et qui..

+ Imprime "impair" si le nombre est impair
+ Imprime "petit nombre pair" si le nombre est pair et inclus entre 0 et 20.
+ Imprime "grand nombre pair" si le nombre est pair et inclus entre 20 et 100.
+ Imprime "nombre pair et negatif" si le nombre est pair et strictement négatif.


== Carré Parfait
Écrire un programme qui verifie si un nombre est un #link-carre-parfait.


== Multiple de 3 et 5

Écrire un programme qui imprime les 10 premiers multiples de 5 qui sont aussi 
des multiples de 3 (15, 30, 45, ...).


== Nombre Laid
Un nombre laid est un nombre uniquement divisible par 2, 3 ou 5.  Écrire
un programme qui identifie si un nombre est laid.


== Suite de Fibonacci
Écrire un programme qui imprime les 100 premiers #link-fibb.


== Fizz Buzz
Écrire un programme qui, pour les nombres entre 1 et 20 :

- si le nombre est divisible par 3 : imprime "Fizz"
- si le nombre est divisible par 5 : imprime "Buzz"
- si le nombre est divisible par 3 et par 5 : imprime "Fizzbuzz"
- sinon : on écrit le nombre

== Palindrome
Écrire un programme qui vérifie si un mot est un #link-palindome.  On peut
aussi être intéressé à vérifier si un mot est un "presque palindrome",
c'est-à-dire un palindrome si l'on enlève tous les charactères qui ne sont 
pas des letters latines.

== Text Dans Un Cadre

Écrire un programme qui encadre un texte dans un carré de `#`.


Exemple.

```
###############################################################################
#     Pariatur molestiae sint optio commodi incidunt et non. Velit vitae      #
###############################################################################
```

== Transformateur d'Heure

Écrire un programme qui transforme une #link-us-timeformat à une heure de format internationale. 
Par example, `01:15 PM` #sym.arrow.r `13:15`. 

*Trucs & Astuces*

- #link("https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/indexOf")[indexOf]
- #link("https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/substring")[substring]
- #link("https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/parseInt")[parseInt]


== Mort à l'impérialisme

=== Convertisseur de Température
Écrire un programme qui convertie un température de Fahrenheit en Celsius.

=== Convertisseur de Poids
Écrire un programme qui convertie un poids de livre en kg.

=== Convertisseur de Mesures
Écrire un programme qui convertie une mesure de pieds et pouce en metre.

=== Convertiseur de mesures impériales en mesures métriques

Écrire un programme qui convertie un string représentant un poid, une température 
ou une mesure du système impérial au système métrique.

*Trucs & Astuces*
Écrire trois fonctions `isTemperature`, `isWeight` et `isLength`.

*Point Bonus*
Vous allez avoir des points bonus si vous êtes capable de convertir de
pieds et pouces de formats `5′10″`.

== Escalier

Écrire un programme qui imprime un escalier de hauteur $n$.

L'escalier doit monter par la droite (comme ci-bas) et les marches
doit etre faite en `#`. 

Voici un example d'un escalier de hauteur 5.
```
    #
   ##
  ###
 ####
#####
```




