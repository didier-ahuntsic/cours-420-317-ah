#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices

#show: basic-footer
#show: doc => basic-header("Exercices | Faire un site web!", doc)
#let numbering-function(.. nums) = {
    let full = numbering("1.1.a.i", ..nums);
    full.slice(full.position(".") + 1)
}
#set heading(numbering: numbering-function, level: 1)


Nous allons créer un logiciel qui gère une bibliothèque de livres.  Un livre
est un document qui contient:

- un titre 
- un auteur
- un nombre de pages
- une année de publication
- un isbn (qui est un ID)

== Lister tous les livres

=== serveur

Créer un "endpoint" GET à `/books` qui retourne la liste de tous les isbn
dans la bibliothèque.  

```typescript 
// GET /books
{
    "books": [
        {"isbn": "1"},
        {"isbn": "20"},
        {"isbn": "33"},
        {"isbn": "145"}
    ]
}
```

=== client

Créer une méthode qui retourne tous les isbn dans la bibliothèque.


== Créer un livre

=== serveur

Créer un "endpoint" POST à `/books` qui créer un nouveau livre.  Il faut s'assurer que le livre est valide et qu'il n'est pas un duplicata.

```typescript 
// POST /books
{
    "isbn": "345",
    "author": "leon tolstoy",
    "titre": "Hadji Mourat",
    "publishedYear": 1900
}
```

=== Client

==== Fonction générale
Créer une fonction qui créer un livre.  La fonction devrait prendre en 
arguments les 4 champs d'un livre.

==== Fonction util
Créer une fonction qui créer 4 livres automatiquement.


== Description d'un livre

=== Serveur

Créer un "endpoint" GET à `/books/:isbn` qui retourne la description complete
du livre avec l'isbn choisi.

```typescript
// GET /books/345
{
    "isbn": "345",
    "author": "leon tolstoy",
    "titre": "Hadji Mourat",
    "publishedYear": 1900
}
```

=== Client

====

Créer une fonction qui prend un isbn et qui retourne le livre.

====

Créer une fonction qui prend une liste d'isbn et qui retourne
tous les livres sous-jacents.

== Recherche de livre par auteur

=== Serveur

Ajouter au endpoint `/books` la possibilité de lister les livres d'un
auteur en particulier.

```typescript 
// GET /books?author=tolstoy
{
    "books": [
        {"isbn": "145"}
    ]
}
```

=== Client

Créer une fonction qui liste tous les isbn des livres d'un auteur en particulier.

== Recherche de livre par année minimale

=== Serveur

Ajouter au endpoint `/books` la possibilité de lister les livres au moins
aussi récents qu'une certaine année.

```typescript 
// GET /books?minYear=1950
{
    "books": [
        {"isbn": "23"},
        {"isbn": "12"},
    ]
}
```

=== Client

Créer une fonction qui liste tous les isbn des livres plus 
récent qu'une certaine année.

== Recherche de livres par auteur et par année minimale

=== Client

Créer une fonction qui liste tous les isbn des livres plus 
récents qu'une certaine année et écrit par un auteur en 
particulier.


== Emprunter

=== Serveur

Ajouter au endpoint POST `/books/:isbn` la possibilité d'emprunter un
livre.  Lorqu'un livre est emprunté, il ne doit plus être visible dans
la liste des isbn.

Si le livre est déjà emprunté, le endpoint devrait retourner un 
status `400`.


```typescript 
// GET /books/234
{
    "action": "rentBook",
    "userId": 34,
}
```

=== Client

Créer une fonction qui prend en argument un isbn et un userId et qui
réserve un livre.

== Lister les usagers de la bibliothèque

=== Serveur

Créer un "endpoint" GET à `/users` qui liste tous les 
usagés de la bibliothèque.

```typescript 
// GET /users
{"users": [{"userId": "234"},
           {"userId": "34"},
           {"userId": "100"},
          ]
}
```

=== Client

Créer une fonction qui liste tous les usagé de la bibliothèque.

== Détail d'un usagé

=== Serveur

Créer un "endpoint" GET à `/users/:userId` qui donne tous les détails
d'un usagé.

```typescript 
// GET /users/666

{"userId": "234",
 "rentedBooks": [
    {"isbn": "23"},
    {"isbn": "12"},
 ]
}
```

== Retourner un livre

=== Serveur

Créer un "endpoint" DELETE à `/users/:userId/rented-books/:isbn` qui retourne
le livre `:isbn` de l'utilisateur `:userId`.

```typescript 
// DELETE /users/666/rented-book/:isbn

```

=== Client 

Créer une fonction qui retourne le livre d'un utilisateur.