{
  description = "The 420-306-AH course";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    reveal.url = "github:hakimel/reveal.js/4.5.0";
    reveal.flake = false;
  };

  outputs = { self, nixpkgs, flake-utils, reveal }:
     flake-utils.lib.eachDefaultSystem (system:
       let
         pkgs = nixpkgs.legacyPackages.${system};
         git-hash = if (self ? rev) then builtins.substring 0 8 self.rev else "dirty";
         last-updated = if (self ? lastModified) then builtins.toString self.lastModified else "0";
         
         public-exams = pkgs.stdenv.mkDerivation {
          name = "public-exams";
          src = ./documents;
          buildInputs = with pkgs; [typst yq git];
          buildPhase = ''
          export LAST_UPDATED_STRING=`date -u +"%Y-%m-%dT%H:%M:%SZ"  -d @${last-updated}`
          tomlq --toml-output  ".language = \"fr\" | .githash = \"${git-hash}\" | .lastUpdated = \"$LAST_UPDATED_STRING\"" main.toml > main-tmp.toml
          cp main-tmp.toml main.toml
          mkdir -p dest
          typst --root . compile examens/pre-test/main.typ dest/pre-test.pdf
          typst --root . compile examens/travail-pratique-001.typ dest/travail-pratique-1.pdf
          typst --root . compile examens/travaille-pratique-002.typ dest/travail-pratique-2.pdf
          typst --root . compile cheatsheets/http.typ dest/cheatsheets-http.pdf
          typst --root . compile examens/travaille-pratique-003.typ dest/travail-pratique-3.pdf

          '';
          installPhase = '' 
          cp -r dest $out
          cp main.toml $out
          '';
         };

         exercices = pkgs.stdenv.mkDerivation {
          name = "exercices";
          src = ./documents;
          buildInputs = with pkgs; [typst yq git];
          buildPhase = ''
          export LAST_UPDATED_STRING=`date -u +"%Y-%m-%dT%H:%M:%SZ"  -d @${last-updated}`
          tomlq --toml-output  ".language = \"fr\" | .githash = \"${git-hash}\" | .lastUpdated = \"$LAST_UPDATED_STRING\"" main.toml > main-tmp.toml
          cp main-tmp.toml main.toml
          mkdir -p dest
          touch dest/_bogus.pdf
          find  exercices -name '*.typ' -exec typst --root . compile {} \;
          '';
          installPhase = '' 
          cp -r dest $out
          cp main.toml $out
          '';
         };
         
         notes-de-cours = pkgs.stdenv.mkDerivation {
          name = "nodes-de-cours";
          src = ./cours;
          buildInputs = [pkgs.jupyter 
                         pkgs.babashka
                         pkgs.brotli
                         pkgs.gzip
                         public-exams];
          buildPhase = ''
          # -SlidesExporter.reveal_theme=black would use dark theme but the css
          # would need a lot of work.
          cp -r ${reveal} reveal.js
          jupyter-nbconvert  *.ipynb --to slides --reveal-prefix reveal.js
          jupyter-nbconvert  *.ipynb --to html
          mkdir -p _exams

          mkdir -p _exercices
          cp ${exercices}/*.pdf _exercices
          cp ${public-exams}/*.pdf _exams
          bb index.clj --courses *.slides.html --exams _exams/*.pdf --exercices _exercices/*.pdf > index.html
          find . -type f -regex '.*\.\(htm\|html\|pdf\|js\|css\)$' -exec brotli -9 -f -k {} \;
          find . -type f -regex '.*\.\(htm\|html\|pdf\|js\|css\)$' -exec gzip -9 -f -k {} \;
          '';
          installPhase = ''
          mkdir -p $out
          cp -r . $out
          '';
          GIT_HASH = git-hash;
          LAST_UPDATED=last-updated;
        };

        correction = pkgs.stdenv.mkDerivation {
           name = "correction";
           src = ./documents;
           buildInputs = with pkgs; [typst yq git];
           buildPhase = ''
           export LAST_UPDATED_STRING=`date -u +"%Y-%m-%dT%H:%M:%SZ"  -d @${last-updated}`
           tomlq --toml-output  ".language = \"fr\" | .githash = \"${git-hash}\" | .lastUpdated = \"$LAST_UPDATED_STRING\"" main.toml > main-tmp.toml
           cp main-tmp.toml main.toml
           find  examens/corrections -name '*.typ' -exec typst --root . compile {} \;
           find  examens/corrections ! -name '*.pdf' -type f -exec rm {} \;
           mv examens/corrections dest
          '';
          installPhase = '' 
          cp -r dest $out
          cp main.toml $out
          '';

        };
        server = pkgs.writeShellApplication {
          name = "note-de-cours-server";
          runtimeInputs = [
            notes-de-cours 
            pkgs.nodePackages_latest.http-server
          ];
          text = ''
          ${pkgs.nodePackages_latest.http-server}/bin/http-server ${notes-de-cours}
          '';
        };
        correction-server = pkgs.writeShellApplication {
          name = "correction-server";
          runtimeInputs = [
            correction
            pkgs.nodePackages_latest.http-server
          ];
          text = ''
          ${pkgs.nodePackages_latest.http-server}/bin/http-server ${correction}
          '';
        };
        python = pkgs.python3.withPackages(ps: with ps; [numpy
                                                         pandas
                                                         requests
                                                         ipdb
                                                         matplotlib
                                                         duckdb
                                                         pyarrow
                                                         jupyter
                                                         pytest
                                                         faker]);
       in
       {
         packages = {
          notes-de-cours = notes-de-cours;
          server = server;
          exams = public-exams;
          exercices = exercices;
          correction = correction;
          correction-server = correction-server;
         };
         devShells.default = pkgs.mkShell {
            buildInputs = with pkgs; [typst
                                      babashka
                                      texlive.combined.scheme-tetex
                                      dotnet-sdk_7
                                      python];

         };
       }
     );
}
